# Chemistry A levels
__Important__: All the equations and internal links work only in [Obsidian's](https://obsidian.md) Preview Mode.

__To download a self updating version__:
1. Download and install git. For instrutions, use this [guide](https://www.linode.com/docs/guides/how-to-install-git-on-linux-mac-and-windows/).
2. Go to the folder where you want to place the notes and open that folder in the terminal.
3. Then type in `git clone https://gitlab.com/TheSilentDefender/chemistry-a-level-notes.git`
4. Then you can open obsidian and select the folder. When prompted, turn off safe mode to use the plugins downloaded with the notes. For more themes, go to Settings → Apperance → Browse Community Themes. 

__Note that this will be incomplete and this is for the syllabus for 2022__

# Topics
1. [Lattice Energy](Lattice%20Energy.md)
2. [Entropy and Gibbs Free Energy](Entropy%20and%20Gibbs%20Free%20Energy.md)



