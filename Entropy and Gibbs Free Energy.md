# Entropy and Gibbs Free Energy
## Entropy
__Definition__: It is the measure of dispersal of energy at a specific temperature and the measure of disorder of a system. Its unit is $JKmol^{-1}$

Entropy can also be thought of as the number of ways particles and energy can be distributed.

The atoms in a solid are thightly paxked and confined. They can only vibrate in thier fixed position. The number of possible ways for the particles and energy to be distributed is less. Hence, the solid will have low entropy. 

However, the atoms in a gas are free to move around the space that the gas occupies. This means that the gas is very disordered and there a more possible wats of distribution. Hence, the gas will have a high level of entropy.

Note that all standard molar entropies are postive.

### Entropy trend among states
```mermaid
graph TD

 A[Solid] 

 B[Liquid]

 C[Gas]

 A -->B

 B --> C
 ```
 The entropy increases from solid to gas.
 
 ## Calculating entropy changes
 
 