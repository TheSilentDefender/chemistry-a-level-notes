# Lattice Energy
__Definition:__ Lattice energy is the enthalpy change when one mole of an ionic compound is formed from its gaseous ions under standard conditions. 

$(g) \rightarrow (s)$

Lattice energy is always exothermic

$\triangle H^\ominus _{latt}$

Some examples:

$Na^+(g)+Cl^-(g)\rightarrow NaCl(s) \space \triangle H^\ominus _{latt}=-787Kg\space mol^{-1}$

- [Enthalpy change of atomization](##%20Enthalpy%20Change%20of%20Atomization)
- [Electron Affinity](##%20Electron%20Affinity)
- [Born-Haber Cycle](##%20Born-Haber%20Cycle)


## Enthalpy Change of Atomization
__Definition__: The Enthalpy change of one mole of gaseous atoms is fromed from its elements under standard conditions.

__Equations examples__: 

$\frac{1}{2} Cl_2(g) \rightarrow Cl(g) \space \space \triangle H^\ominus _{at}=+122Kg\space mol^{-1}$

Enthalpy change of atomization is always positive, as energy is required to break the bonds of the atoms of the element.

## Electron Affinity
__Definition__: The enthalpy change when one mile of electrons is added to one mole of gaseous atoms to form one mole of -1 changed ions under standard conditions.

Electron affinities are generally negative, and the second and third election affinities are always positive.

__Equation Examples__:

$\frac{1}{2} Cl(g) + e^- \rightarrow Cl^-(g) \space \space \triangle H^\ominus _{EA}=-348Kg\space mol^{-1}$

$\frac{1}{2} S(g) + e^- \rightarrow S^-(g) \space \space \triangle H^\ominus _{EA}=-200Kg\space mol^{-1}$

$\frac{1}{2} O(g) + e^- \rightarrow O^-(g) \space \space \triangle H^\ominus _{EA}=-141Kg\space mol^{-1}$

$\frac{1}{2} O^-(g) + e^- \rightarrow O^{-2}(g) \space \space \triangle H^\ominus _{EA_2}=+798Kg\space mol^{-1}$

## Born-Haber Cycle
```mermaid
graph TD

 A[Ions in gaseous state] 

 B[Ionic Compound]

 C[Elements in standard state]

 A -->|Lattice Energy|B

 C -->|Enthalpy Change of Formation|B

 C --> |Ionisation Energy|A
 ```
 
 $\triangle H^\ominus _{latt}=\triangle H^\ominus _{f}-\triangle H^\ominus _{I}$
 
 Note that $\triangle H^\ominus _{I}$ is a multistep process.
 
## Factors affecting lattice energy
 1. [Ion size](###%20Ion%20size)
 2. [Charge of the ion](###%20Charge%20of%20ion)

### Ion size:
Lattice energy becomes less exothermic when ion size increases. Ions with the same charge density have a lower charge density as it has the same charge spread around a larger volume.

```mermaid
graph TD

 A[Low chnarge density] 

 B[Weak electrostatic force of attraction]

 C[Lesser lattice energy]

 A -->B

 B --> C
 ```
 
### Charge of ion
 The higher the charge, the higher the lattice energy because a high charge means that it is a higher charge density. 
 
 ```mermaid
graph TD

 A[High charge density] 

 B[Strong electrostatic force of attraction]

 C[High lattice energy]

 A -->B

 B --> C
 ```
 
## Ion Polarization
 __Definition__: It is the distortion of an anion when the positive charge on a cation attracts the election from the anion towards itself.
 
### Factors affecting ion polarisation
1. Charge Density
2. Polarizability of the anion

#### Polarisability

Polarisability gives a measure of the ability of the electron cloud of a species(element) to be distorted. A species has more polarisability if it:
- The cation is small
- The cation has a charge of +2 or +3
- The anion is large
- The anion has a charge of -2 or -3

 Note that the greater the polarisability of an anion, the easier it is to break/weaken the bond.
 
## Enthalpy change of solution
__Definition__: The energy absorbed or released when one mole of an ionic solid dissolves in enough  water to form a very dilute soltion.
The solid is likely to be soluble if $\triangle H^\ominus _{sol}$ is negative or a small postive value

## Enthalpy change of hydration
__Definition__: The enthalpy change when one mole of a specific gaseous ion dissovles in enough water to form a very dilute solution.
$\triangle H^\ominus _{hyd}$ is always negative since energy is released when forming ion-diople bonds. It is also more exothermic for ions with smaller radii and higher charge.

## Enthalpy change in a solution cycle

```mermaid
graph TD

 A[Gaseous ions] 

 B[Ions in aqueous solutions]

 C[Ionic solid]

 A -->|Hydration|B

 C -->|Solution|B

 A --> |Lattice energy|C
 ```
 
 $\triangle H^\ominus _{hyd}=\triangle H^\ominus _{sol}+\triangle H^\ominus _{latt}$